local rt = RoosterTools
rt.gui = {
    scrollingFrame = {}
}

-- Define colors to be used with frames.
rt.gui.COLORS = {
    ["white"] = {
        ["r"] = 0.8,
        ["g"] = 0.8,
        ["b"] = 0.8,
        ["a"] = 1.0
    },
    ["black"] = {
        ["r"] = 0.0,
        ["g"] = 0.0,
        ["b"] = 0.0,
        ["a"] = 1.0
    },
    ["red"] = {
        ["r"] = 1.0,
        ["g"] = 0.8,
        ["b"] = 0.8,
        ["a"] = 1.0
    },
    ["green"] = {
        ["r"] = 0.8,
        ["g"] = 1.0,
        ["b"] = 0.8,
        ["a"] = 1.0
    },
    ["highlight"] = {
        ["r"] = 0.8,
        ["g"] = 0.8,
        ["b"] = 0.8,
        ["a"] = 0.5
    }
}

rt.gui.createButton = function(name, parent, height, width, text, point, offsetx, offsety, click)
    local button = CreateFrame("Button", name, parent)
    button:SetPoint(point, parent, offsetx, offsety)
    button:SetHeight(height)
    button:SetWidth(width)
    button:SetText(text)
    button:SetNormalFontObject("GameFontNormal")

    local ntex = button:CreateTexture()
    ntex:SetTexture("Interface/Buttons/UI-Panel-Button-Up")
    ntex:SetTexCoord(0, 0.625, 0, 0.6875)
    ntex:SetAllPoints()
    button:SetNormalTexture(ntex)

    local htex = button:CreateTexture()
    htex:SetTexture("Interface/Buttons/UI-Panel-Button-Highlight")
    htex:SetTexCoord(0, 0.625, 0, 0.6875)
    htex:SetAllPoints()
    button:SetHighlightTexture(htex)

    local ptex = button:CreateTexture()
    ptex:SetTexture("Interface/Buttons/UI-Panel-Button-Down")
    ptex:SetTexCoord(0, 0.625, 0, 0.6875)
    ptex:SetAllPoints()
    button:SetPushedTexture(ptex)

    button:SetScript("OnClick", click)

    return button
end

rt.gui.createCheckbox = function(name, parent, text, tooltip, point, offsetx, offsety, click) 
    local checkbox = CreateFrame("CheckButton", name, parent, "ChatConfigCheckButtonTemplate");
    checkbox:SetPoint(point, parent, offsetx, offsety)
    _G[checkbox:GetName() .. 'Text']:SetText(text);
    checkbox.tooltip = tooltip;

    if (click) then
        checkbox:SetScript("OnClick", click) 
    end

    return checkbox
end

rt.gui.createTextBox = function(name, parent, point, relativeTo, relativePoint, offsetx, offsety, keyup)
    local textbox = CreateFrame("EditBox", name, parent)
    textbox:SetPoint(point, relativeTo, relativePoint, offsetx, offsety)
    textbox:SetAutoFocus(false)
    textbox:SetFontObject("ChatFontNormal")
    textbox:SetSize(150, 22)
    textbox:SetTextInsets(10, 10, 10, 10)
    Mixin(textbox, BackdropTemplateMixin)
    textbox.backdropInfo = BACKDROP_TEXT_PANEL_0_16
    textbox:OnBackdropLoaded()

    if (keyup) then
        textbox:SetScript("OnKeyUp", keyup)
    end

    return textbox
end

rt.gui.createLabel = function(name, parent, text, point, relativeTo, relativePoint, offsetx, offsety) 
    local label = parent:CreateFontString(name, "OVERLAY", "GameTooltipText")
    label:SetText(text)
    label:SetPoint(point, relativeTo, relativePoint, offsetx, offsety)
    return label
end

-------------------------------------------------------------------------------
-- Scrolling frame functions
-------------------------------------------------------------------------------
rt.gui.scrollingFrame.buildColumn = function(t)
    local t = t

    local template = {
        ["name"] = "#",
        ["width"] = 15,
        ["align"] = "RIGHT",
        ["color"] = rt.gui.COLORS.white,
        ["colorargs"] = nil,
        ["bgcolor"] = rt.gui.COLORS.black,
        ["DoCellUpdate"] = nil
    }

    for k, v in pairs(t) do
        template[k] = v
    end

    return template
end