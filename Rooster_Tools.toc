## Interface 90001
## Title: Rooster Tools
## Notes: Guild Participation Toolbox
## Author: UngnomeKnown
## Version: 0.19-beta
## SavedVariables: RoosterToolsDB

embeds.xml

Util.lua
Group.lua
Character.lua

Core.lua

GUI.lua
Comm.lua
DB.lua
Chat.lua
Event.lua
Frame.lua
MiniMap.lua
WaitList.lua
