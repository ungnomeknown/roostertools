-- Data provider for the minimap icon
local ldb = LibStub("LibDataBroker-1.1"):NewDataObject("RoosterToolsLDB", {
    type = "data source",
    text = "Rooster Tools",
    OnTooltipShow = function(tooltip)
        tooltip:SetText("Rooster Tools")
        tooltip:AddLine("Click to show/hide", 1, 1, 1)
        tooltip:Show()
    end,
    icon = "Interface\\Addons\\Rooster_Tools\\Resources\\pexels-pixabay-34770_64x64.tga",
    OnClick = function()
        if (RoosterTools.rootFrame:IsVisible()) then
            RoosterTools.rootFrame:Hide()
        else
            RoosterTools.rootFrame:Show()
        end
    end
})

-- Attach the icon to the minimap
local icon = LibStub("LibDBIcon-1.0")
icon:Register("Rooster Tools", ldb, {
    hide = false
})

RoosterTools.minimapIcon = icon