local rt = RoosterTools

--------------------------------------------------------------------------
-- Attach event handlers to the RoosterTools global.
--------------------------------------------------------------------------
RoosterToolsEvent = {
    lastGuildUpdate = time() - 60
}

function RoosterToolsEvent.GROUP_ROSTER_UPDATE()
    PartyWatch:UpdateDB()
end

function RoosterToolsEvent.GUILD_ROSTER_UPDATE()
    if (rt.nps.inCombat) then
        return
    end

    if (time() - RoosterToolsEvent.lastGuildUpdate < 47) then
        return
    end

    RoosterToolsEvent.lastGuildUpdate = time()

    RoosterTools:GuildRosterTableUpdate()
end

function RoosterToolsEvent.GROUP_FORMED(e, category, partyGuid)
    RoosterToolsDB.partyGuid = partyGuid
end

function RoosterToolsEvent.GROUP_JOINED(e, category, partyGuid)
    RoosterToolsDB.partyGuid = partyGuid
end

function RoosterToolsEvent.GROUP_LEFT(e)
    if (not IsInGroup()) then
        RoosterToolsDB.partyGuid = nil
    end
end

function RoosterToolsEvent.ENCOUNTER_START(e, encounterID, encounterName, difficultyID, groupSize)
    local group = RoosterToolsDB.groups[RoosterToolsDB.partyGuid]
    if (not group) then
        return
    end

    table.insert(group.encounters, {
        instance = group.currentInstance,
        encounterID = encounterID,
        encounterName = encounterName,
        difficultyID = difficultyID,
        groupSize = groupSize,
        started = time()
    })
end

function RoosterToolsEvent.ENCOUNTER_END(e, encounterID, encounterName, difficultyID, groupSize, success)
    local group = RoosterToolsDB.groups[RoosterToolsDB.partyGuid]
    if (not group) then
        return
    end

    -- Find the fight that is currently ending... We need to scan due to older instances allowing multiple fights to
    -- occur in parallel
    local fight = nil
    for k, v in pairs(group.encounters) do
        if (v.encounterID == encounterID and not v.ended) then
            fight = v
        end
    end

    if (fight.encounterID ~= encounterID) then
        logDebug("Uh.... Fight state db out of sync.")
        return
    end

    fight.ended = time()

    if (success == 1) then
        fight.success = true

        if (group.type and group.type == "raid" and rt.player.guildName and rt.player.guildName == "Angry Roosters") then
            rt:ScheduleTimer(function()
            --    PlaySoundFile("Interface\\Addons\\Rooster_Tools\\Resources\\slogan.mp3")
            end, 2)
        end
    else
        fight.success = false
    end
end

function RoosterToolsEvent.PLAYER_LEAVING_WORLD()
    -- RoosterToolsDB.groupsSerialized = RoosterTools:Serialize(RoosterToolsDB.groups)
end

function RoosterToolsEvent.INSPECT_READY(event, unitId, x) 
    if (rt.nps.inCombat) then 
        return
    end

    local members = PartyWatch:ForEach(function(unitId) 
        rt.util.logDebug("Inspecting "..unitId)
        local ilvl = C_PaperDollInfo.GetInspectItemLevel(unitId)
        if (ilvl) then
            return ilvl
        else
            return 0
        end
    end)

    for fullName, ilvl in pairs(members) do
        if (ilvl and ilvl > 0) then
            if (not rt.db.ilvl[fullName] or rt.db.ilvl[fullName].ilvl ~= ilvl) then
                rt.db.ilvl[fullName] = {
                    ["ilvl"] = ilvl,
                    ["updated"] = time()
                }
            end
        end
    end
end

function RoosterToolsEvent.PLAYER_ENTER_COMBAT()
    RoosterTools.nps.inCombat = true
end

function RoosterToolsEvent.PLAYER_LEAVE_COMBAT()
    RoosterTools.nps.inCombat = false
end

-- Attach to all events.
function RoosterToolsEvent:OnInitialize()
    rt:RegisterEvent("GROUP_ROSTER_UPDATE", RoosterToolsEvent.GROUP_ROSTER_UPDATE)
    rt:RegisterEvent("GROUP_FORMED", RoosterToolsEvent.GROUP_FORMED)
    rt:RegisterEvent("GROUP_JOINED", RoosterToolsEvent.GROUP_JOINED)
    rt:RegisterEvent("GROUP_LEFT", RoosterToolsEvent.GROUP_LEFT)

    rt:RegisterEvent("ENCOUNTER_START", RoosterToolsEvent.ENCOUNTER_START)
    rt:RegisterEvent("ENCOUNTER_END", RoosterToolsEvent.ENCOUNTER_END)

    rt:RegisterEvent("GUILD_ROSTER_UPDATE", RoosterToolsEvent.GUILD_ROSTER_UPDATE)

    rt:RegisterEvent("PLAYER_LEAVING_WORLD", RoosterToolsEvent.PLAYER_LEAVING_WORLD)
    rt:RegisterEvent("INSPECT_READY", RoosterToolsEvent.INSPECT_READY)
    rt:RegisterEvent("UNIT_INVENTORY_CHANGED", RoosterToolsEvent.INSPECT_READY)

    rt:RegisterEvent("PLAYER_ENTER_COMBAT", RoosterToolsEvent.PLAYER_ENTER_COMBAT)
    rt:RegisterEvent("PLAYER_LEAVE_COMBAT", RoosterToolsEvent.PLAYER_LEAVE_COMBAT)
end
