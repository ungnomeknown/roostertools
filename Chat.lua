local rt = RoosterTools

RoosterToolsChat = {}

function RoosterToolsChat.SlashProcessor(cmd)
    if (cmd == "show") then
        RoosterToolsHolderFrame:Show()
    elseif (cmd == "hide") then
        RoosterToolsHolderFrame:Hide()
    elseif (cmd == "clearGroups") then
        RoosterToolsDB.groups = {}
        logDebug("Groups cleared!")
    elseif (cmd == "sync") then
        RoosterToolsComm:Send(RoosterToolsComm.TYPES.GUILD_SYNC_REQ, { 
            id = RoosterToolsDB.id, 
            refuseRankBelow = RoosterToolsDB.config.sync.refuseRankBelow 
        }, RoosterToolsComm.METHOD.GUILD)
    elseif (cmd == "syncClear") then
        RoosterToolsComm:Send(RoosterToolsComm.TYPES.SYNC_CLEAR_REQ, { id = RoosterToolsDB.id }, RoosterToolsComm.METHOD.GUILD)
        RoosterToolsUtil.logInfo("Sending sync clear request to online guild participants...")
    elseif (cmd == "versions") then
        RoosterToolsComm:Send(RoosterToolsComm.TYPES.VERSION_REQ, {}, RoosterToolsComm.METHOD.GUILD)
        RoosterToolsUtil.logInfo("Request Versions...")
    else
        logDebug(cmd)
    end
end

function RoosterToolsChat:OnInitialize() 
    rt:RegisterChatCommand("rooster", RoosterToolsChat.SlashProcessor)
end