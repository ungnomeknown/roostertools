PartyWatch = {}

PartyWatch.ROOSTER_STATES = {
    -- This should be changed if we ever need to track proximity.
    XACTIVE = "ACTIVE",
    ACTIVE = "ACTIVE",
    INACTIVE = "INACTIVE",
    LEFT = "LEFT"
}

-- Return a list of party unit ids
function PartyWatch:PartyUnitIds()
    local groupSize = GetNumGroupMembers()

    local units = {}
    units[groupSize + 1] = "player"
    if (IsInGroup()) then
        local type = "party"
        if (IsInRaid()) then
            type = "raid"
        end

        for i = 1, groupSize do
            units[i] = type .. i
        end
    end

    return units
end

function PartyWatch:ForEach(f)
    local members = {}
    for i, unitId in pairs(self:PartyUnitIds()) do
        local unitName, unitRealm = UnitName(unitId)
        if (not unitRealm) then
            unitRealm = GetRealmName()
        end
        if (unitName and unitRealm) then
            local key = unitName.."-"..unitRealm
            if (not members[key]) then
                members[key] = {}
            end

            if (f) then
                members[key] = f(unitId)
            else
                members[key] = {}
            end
        end
    end

    return members
end

local function mergeType(group)
    -- If our party type didn't change ignore...
    local raidGroup = IsInRaid()
    if (raidGroup and group.type == "raid") then
        return
    elseif (not raidGroup and group.type == "party") then
        return
    end

    -- Convert type 
    if (group.type == "party") then
        group.type = "raid"
    else
        group.type = "party"
    end

    -- Minimum number of guild members to make this a guild event.
    local minMembers = RoosterToolsDB.config[group.type].minGuildMembers

    -- If we already meet the minimum participation for this type of group, we're good.
    if (group.guildMembers >= minMembers and not group.guildGroupTime) then
        group.guildGroupTime = time()
    elseif (group.guildMembers < minMembers) then
        group.guildGroupTime = nil
    end
end

PartyWatch.lastUpdate = 0
function PartyWatch:UpdateDB()
    local start = time()
    if (start - self.lastUpdate < 15) then
        return
    end

    self.lastUpdate = start

    -- Create new RoosterToolsMember objects foreach party member
    local members = PartyWatch:ForEach(function(unitId)
        return RoosterToolsMember:New(unitId)
    end)

    PartyWatch:UpdateGroupDB(members)
end

function PartyWatch:UpdateGroupDB(members)
    -- Determine if this is a group that should be calculated for participation.
    local partyGuid = RoosterToolsDB.partyGuid

    -- If we don't know what party we are in... nothing else matters.
    if (not partyGuid) then
        return
    end

    -- Init the group if we haven't already.
    if (not RoosterToolsDB.groups[partyGuid]) then
        RoosterToolsDB.groups[partyGuid] = {
            currentInstance = nil,
            created = time(),
            members = {},
            instances = {},
            encounters = {},
            guildGroupTime = nil,
            guildMembers = 0,
            type = "party"
        }
    end

    local group = RoosterToolsDB.groups[partyGuid]

    -- Know when our last update was... 
    group.updated = time()

    -- Detect party type changes and adjust guild grouping accordingly.
    mergeType(group)

    -- If we are in an instance, init the instance record.
    if (IsInInstance()) then
        local instanceName, type, difficultyIndex, difficultyName, maxPlayers, dynamicDifficulty, isDynamic,
            instanceMapId, lfgID = GetInstanceInfo()

        if (group.currentInstance ~= instanceName) then
            group.currentInstance = instanceName

            table.insert(group.instances, {
                created = time(),
                instanceName = instanceName,
                type = type,
                difficultyIndex = difficultyIndex,
                difficultyName = difficultyName,
                maxPlayers = maxPlayers,
                dynamicDifficulty = dynamicDifficulty,
                isDynamic = isDynamic,
                instanceMapId = instanceMapId,
                lfgID = lfgID
            })
        else
            -- Update the instance so that we know the last time the addon saw the player in the instance.
            -- TODO FIX THIS
            group.instances[#group.instances].updated = time()
        end
    end

    -- Current group members.
    local guildMembers = 0
    for k, v in pairs(members) do
        local guildMember = RoosterTools.nps.guildRoster[v.unitName]
        if (guildMember) then
            guildMembers = guildMembers + 1
        end

        if (not group.members[k] and guildMember) then
            group.members[k] = {
                state = nil,
                states = {}
            }
        end

        if (guildMember) then
            -- Determine current activity state.
            local currentState = self.ROOSTER_STATES.ACTIVE
            if (v.isAFK or not v.isConnected) then
                currentState = self.ROOSTER_STATES.INACTIVE
            end

            local member = group.members[k]
            if (member.state ~= currentState) then
                member.state = currentState
                table.insert(group.members[k].states, {
                    state = currentState,
                    time = time()
                })
            end
        end
    end

    -- Update the number of members from the players guild.
    group.guildMembers = guildMembers

    -- Set the guild group time for later calcs
    if (not group.guildGroupTime and group.guildMembers >= RoosterToolsDB.config[group.type].minGuildMembers) then
        logDebug("Guild Group, Congrats!")
        group.guildGroupTime = time()
    end

    -- Group members that have left.
    for k, v in pairs(group.members) do
        if (not members[k] and v.state ~= self.ROOSTER_STATES.LEFT) then
            local currentState = self.ROOSTER_STATES.LEFT
            v.state = currentState
            table.insert(v.states, {
                state = currentState,
                time = time()
            })
        end
    end
end
