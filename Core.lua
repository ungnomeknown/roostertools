RoosterTools = LibStub("AceAddon-3.0"):NewAddon("RoosterTools", "AceConsole-3.0", "AceEvent-3.0", "AceTimer-3.0",
                   "AceComm-3.0", "AceSerializer-3.0")
RoosterTools.version = GetAddOnMetadata("Rooster_Tools", "Version")

-- Non-persistent storage.
RoosterTools.nps = {
    guildRoster = {},
    mainCleanupComplete = false,
    inCombat = false
}

local db = nil
local dbconn = nil 
local util = RoosterToolsUtil

-- Mainception can occur if the alt points at a main and a main points to an alt. 
-- We fix this by clearing both sides of the relationship and it'll resolve with the guild note.
function RoosterTools:MainCeptionFix()
    for k, v in pairs(RoosterTools.nps.guildRoster) do
        local main = RoosterToolsDB.mains[v.nameOnly]
        
        if (main and main.name) then
           local mainCeption = RoosterToolsDB.mains[main.name]
           if (mainCeption and mainCeption.name) then
              main.name = nil
              main.time = time()
              mainCeption.name = nil
              mainCeption.time = time()
           end
        end
    end
end

function RoosterTools:GroupCleanup(guid)
    local g = db.groups[guid]

    if (not g) then
        return
    end

    local now = time()
    if (now - g.updated > 1440 and util.count(g.instances) == 0) then
        db.groups[guid] = nil
    else
        -- This can go away after a couple versions. 
        local dumpIt = {}
        for guid, member in pairs(g.members) do
            local player = db.characters[guid]
            if (player) then
                local realm = player.unitRealm or GetRealmName()
                g.members[player.unitName.."-"..realm] = member

                dumpIt[guid] = true
            end
        end

        for guid, _ in pairs(dumpIt) do
            g.members[guid] = nil
        end

        -- db.characters = {}
        -- db.charactersIndex = {}
    end
end

function RoosterTools:GroupReview(group)
    local guilds = {}

    local findJoin = function(events)
        for k, v in pairs(events) do
            if (v.state == "JOIN_GROUP") then
                return v
            end
        end
    end

    local t = {}
    table.insert(t, group)
    for k, v in pairs(group.members) do
        local events = RoosterTools.GroupEventStream(k, t)
        local joinedEvent = findJoin(events)
        
        local realm = v.unitRealm or self.player.playerRealm
        local nameOnly = ""
        if (v.unitName) then
            nameOnly = v.unitName:gmatch("[^-]+")()
        end
        if ((self.nps.guildRoster[v.unitName] or self.nps.guildRoster[nameOnly]) and joinedEvent) then
            table.insert(guilds, joinedEvent.time)
        end
    end

    -- Calulate when this group became a guild group and for which guild.
    local guildGroupTime = 9007199254740994
    local guildName = nil
    local guildMembers = nil

    local count = util.count(guilds)
    if (count >= db.config[group.type].minGuildMembers) then
        table.sort(guilds)
        local groupTime = guilds[db.config[group.type].minGuildMembers]
        guildGroupTime = math.min(guildGroupTime, groupTime)
        guildName = RoosterTools.player.guildName
        guildMembers = count
    end

    -- Update the group with a guild group time and guild name.
    if (guildName) then
        group.guildName = guildName
        group.guildGroupTime = guildGroupTime
        group.guildMembers = guildMembers
    elseif (group.guildGroupTime) then
        group.guildGroupTime = nil
        group.guildName = nil
    end
end

-- Sort chronologically.
function RoosterTools.SortEvents(events)
    if (not events) then
        return
    end

    local sortRank = {
        JOIN_GROUP = 0,
        ACTIVE = 1,
        INSTANCE_START = 2,
        ENCOUNTER_START = 3,
        ENCOUNTER_END = 4,
        INSTANCE_END = 5,
        GROUP_END = 6,
    }

    table.sort(events, function(a, b)
        if (a.time == b.time) then
            if ((sortRank[a.state] or 0) < (sortRank[b.state] or 0)) then
                return true
            else 
                return false
            end
        elseif (a.time < b.time) then
            return true
        else
            return false
        end
    end)
end

function RoosterTools.GroupEventStream(playerName, groups)
    local allEvents = {}

    -- Find all events that the player was a part of...
    for groupGuid, v in pairs(groups) do
        local member = v.members[playerName]
        if (member) then
            local events = {}

            -- Grab the states of the player to compare with the encounters.
            for k, v in pairs(member.states) do
                table.insert(events, {
                    state = v.state,
                    time = v.time
                })
            end

            RoosterTools.SortEvents(events)

            table.insert(events, {
                state = "JOIN_GROUP",
                time = events[1]["time"] or 0
            })

            -- Get all the possible encounters and add them to the timeline.
            local maxEnd = 0
            for k, v in pairs(v.encounters) do
                table.insert(events, {
                    state = "ENCOUNTER_START",
                    time = v.started,
                    encounterName = v.encounterName
                })

                -- It's possible that the encounter never ended.
                if (v.ended) then
                    table.insert(events, {
                        state = "ENCOUNTER_END",
                        time = v.ended,
                        encounterName = v.encounterName
                    })

                    -- No need to allow group participation to go beyond the last ended pull of the night.
                    if (v.ended > maxEnd) then 
                        maxEnd = v.ended
                    end
                end
            end

            table.insert(events, {
                state = "GROUP_END", 
                time = v.updated, 
                instance = v.currentInstance, 
                created = v.created 
            })

            for k, v in pairs(v.instances) do
                local instanceEnd = v.updated or v.created
                if (instanceEnd > maxEnd and maxEnd > 0) then
                    instanceEnd = maxEnd
                end

                table.insert(events, {
                    state = "INSTANCE_START",
                    type = v.type,
                    time = v.created,
                    instance = v.instanceName,
                    created = v.created
                })

                table.insert(events, {
                    state = "INSTANCE_END",
                    time = maxEnd,
                    type = v.type,
                    instance = v.instanceName,
                    created = v.created,
                    groupGuid = groupGuid
                })
            end

            RoosterTools.SortEvents(events)
            for k, v in pairs(events) do
                table.insert(allEvents, v)
            end
        end
    end
    
    return allEvents
end

function RoosterTools:Calc(playerName)
    local results = {}

    local lastState = PartyWatch.ROOSTER_STATES.INACTIVE
    local lastEventTime = 0
    local activeTime = 0
    local xActiveTime = 0
    local inactiveTime = 0
    local guildGroups = dbconn.SelectGuildGroups()
    local events = self.GroupEventStream(playerName, guildGroups)

    local buff = ""
    for k, v in pairs(events) do
        -- buff = buff .. k .. " ".. v.state .. " " ..v.time .. " " 
        -- if (v.type) then buff = buff .. v.type end
        -- buff = buff .. "\n"

        if (lastEventTime == 0) then
            lastEventTime = v.time
        end
        
        if (v.state == PartyWatch.ROOSTER_STATES.ACTIVE) then
            activeTime = activeTime + difftime(v.time, lastEventTime)
            lastState = v.state
        elseif (v.state == PartyWatch.ROOSTER_STATES.XACTIVE) then
            xActiveTime = xActiveTime + difftime(v.time, lastEventTime)
            lastState = v.state
        elseif (v.state == PartyWatch.ROOSTER_STATES.INACTIVE or v.state == "LEFT") then
            inactiveTime = inactiveTime + difftime(v.time, lastEventTime)
            lastState = v.state
        elseif (lastState ~= PartyWatch.ROOSTER_STATES.INACTIVE) then
            if (lastState == PartyWatch.ROOSTER_STATES.XACTIVE) then
                xActiveTime = xActiveTime + difftime(v.time, lastEventTime)
            elseif (lastState == PartyWatch.ROOSTER_STATES.ACTIVE) then
                activeTime = activeTime + difftime(v.time, lastEventTime)
            end
        end

        lastEventTime = v.time

        if (v.state == "GROUP_END") then
            lastState = PartyWatch.ROOSTER_STATES.INACTIVE
            lastEventTime = 0
            activeTime = 0
            xActiveTime = 0
            inactiveTime = 0
        elseif (v.state == "INSTANCE_END") then
            table.insert(results, {
                ["created"] = v.created,
                ["instance"] = v.instance,
                ["instanceType"] = v.type,
                ["activeTime"] = activeTime,
                ["xActiveTime"] = xActiveTime,
                ["inactiveTime"] = inactiveTime,
                ["groupGuid"] = v.groupGuid
            })

            if (playerName == "Ungnomeknown-Nesingwary" and v.type == "raid") then
                --print(v.created, v.instance, v.type, activeTime, xActiveTime, inactiveTime, v.groupGuid)
            end

            lastState = PartyWatch.ROOSTER_STATES.INACTIVE
            lastEventTime = 0
            activeTime = 0
            xActiveTime = 0
            inactiveTime = 0
        end
    end
    db.temp = buff
    return results
end

function RoosterTools:GuildRosterTableUpdate()
    local groups = dbconn:SelectGuildRaidGroups()
    local totalRaids = util.count(groups)

    local numTotalMembers, numOnlineMaxLevelMembers, numOnlineMembers = GetNumGuildMembers();

    local guildRoster = {}
    for i = 1, numTotalMembers do
        local name, rank, rankIndex, level, class, zone, note, officernote, online, status, classFileName,
            achievementPoints, achievementRank, isMobile, isSoREligible, standingID = GetGuildRosterInfo(i);

        if (name) then
            local member = {
                name = name, 
                nameOnly = name:gmatch("[^-]+")(),
                rank = rank, 
                rankIndex = rankIndex, 
                note = note, 
                officernote = officernote, 
                online = online, 
            }

            guildRoster[member.nameOnly] = member
        end
    end

    -- For the sake of efficiency, we store the guild roster here for quick lookups in other modules.
    RoosterTools.nps.guildRoster = guildRoster

    local mains = {}
    local roster = {}
    for k, v in pairs(guildRoster) do
        local name = v.name
        local rank = v.rank
        local rankIndex = v.rankIndex
        local note = v.note
        local officernote = v.officernote

        if (name) then
            local nameOnly = v.nameOnly
            local ilvl = ""
            if (db.ilvl[name]) then
                ilvl = db.ilvl[name].ilvl
            end

            -- Update alias from the guild note if Possible
            local guildNote = note:gsub("%[", ""):gsub("%]", "")
            if ((not db.mains[nameOnly] or db.mains[nameOnly].name ~= guildNote) 
                 and guildNote and guildNote ~= "" and guildRoster[guildNote]
                 and nameOnly ~= guildNote) then
                db.mains[nameOnly] = { 
                    ["name"] = guildNote,
                    ["time"] = time()
                }
            end

            if (not guildRoster[guildNote] and db.mains[guildNote]) then
                db.mains[guildNote] = nil
            end

            if (not RoosterTools.nps.mainCleanupComplete) then
                for k, v in pairs(RoosterToolsDB.mains) do
                    if (not guildRoster[v.name] and v.name) then
                        v.name = nil
                        v.time = time()
                    end
                end

                RoosterTools.nps.mainCleanupComplete = true
            end

            if (nameOnly == guildNote and db.mains[nameOnly]) then
                db.mains[nameOnly] = nil
            end

            -- Determine this toon's main.
            local main = db.mains[nameOnly]
            if (main and main.name) then
                main = main.name
            else
                main = nameOnly
            end

            if (not mains[main]) then
                mains[main] = {
                    raids = {},
                    other = {},
                    score = 0,
                    absent = 0,
                    raidPercent = 0,
                    ilvl = nil
                }
            end

            if (main == nameOnly) then
                mains[main].rankIndex = rankIndex
                mains[main].rank = rank
                mains[main].name = name
                mains[main].ilvl = ilvl
            end

            local results = RoosterTools:Calc(name)

            local score = 0
            local raids = {}
            local other = {}
            if (results) then
                for k, v in pairs(results) do
                    if (v.instanceType == "raid" and v.instance == db.config.raid.currentRaid) then
                        local s = v.activeTime + v.xActiveTime

                        mains[main].score = mains[main].score + s

                        -- Round the score to the nearest point per 15 minutes
                        local mult = 10^0
                        s = s / 900
                        s =  math.floor(s * mult + 0.5) / mult
                        score = score + s

                        -- Count raids at a group level to match up with the Select above.
                        raids[v.groupGuid] = v.instance

                        -- Total the raid only one time per main. 
                        mains[main].raids[v.groupGuid] = 1
                    elseif (v.instanceType == "party") then
                        mains[main].other[v.groupGuid] = 1
                        other[v.groupGuid] = 1
                    end
                end
            end

            local raidCount = util.count(raids)

            local raidPercent = 0
            if (totalRaids > 0) then
                local mainRaids = util.count(mains[main].raids)
                mains[main].raidPercent = math.min(mainRaids, totalRaids) * 100 / totalRaids
                mains[main].absent = totalRaids - mainRaids
                raidPercent = math.min(raidCount, totalRaids) * 100 / totalRaids
            end

            local row = {rankIndex, rank, ilvl or "", main, name, util.count(other), raidCount, totalRaids - raidCount, raidPercent, score}
            table.insert(roster, row)
        end
    end

    local aliasIt = function(tbl) 
        for k, v in pairs(tbl) do
            local alias = db.aliases[v[4]]
            if (alias and alias.alias) then
                v[4] = alias.alias
            end
        end

        return tbl
    end

    -- Set the total guild mains on the frame.
    RoosterTools.rootFrame.guildMembers:SetText(util.count(mains))

    if (RoosterTools.rootFrame.mainsOnly) then
        local mainRoster = {}
        for k, v in pairs(mains) do
            local s = v.score
            -- Round the score to the nearest point per 15 minutes
            local mult = 10^0
            s = s / 900
            s =  math.floor(s * mult + 0.5) / mult
            local row = {v.rankIndex or "", v.rank or "", v.ilvl or "", k, v.name or "", util.count(v.other), util.count(v.raids), v.absent or "", v.raidPercent, s or ""}
            table.insert(mainRoster, row)
        end

        self.guildRosterTable:SetData(aliasIt(mainRoster), true)
    else
        self.guildRosterTable:SetData(aliasIt(roster), true)
    end
end

function RoosterTools:OnInitialize()
    util.logInfo("version: " .. RoosterTools.version)
    self.minimapIcon:Hide()

    -- Init Rooster Tools in a timer so that we have time to get the necessary guild information
    -- for the current player.
    self.initTimer = self:ScheduleRepeatingTimer(function()
        local guildName, _, guildRankIndex = GetGuildInfo("player");
        if (not guildName) then
            return
        end

        -- Request a guild roster update so the addon has data.
        C_GuildInfo.GuildRoster()

        -- Store standard info about the player.
        local guid = UnitGUID("player")
        local playerName = UnitName("player")
        self.player = {
            ["guid"] = guid,
            ["guildName"] = guildName,
            ["playerName"] = playerName,
            ["playerRealm"] = GetRealmName(), 
            ["guildRankIndex"] = guildRankIndex
        }

        self:CancelTimer(self.initTimer)

        RoosterToolsComm:OnInitialize()
        RoosterToolsChat:OnInitialize()
        RoosterToolsEvent:OnInitialize()
        RoosterToolsDBConn:OnInitialize()
        
    
        self.db = RoosterToolsDB
        db = RoosterToolsDB
        self.dbconn = RoosterToolsDBConn
        dbconn = RoosterToolsDBConn
        self.util = util

        RoosterToolsWaitList:OnInitialize()
    
        -- Clear out old state that can get stuck if the user logs out before leaving a party
        if (not IsInGroup()) then
            db.partyGuid = nil
        end
    
        self.groupReviewTimer = self:ScheduleRepeatingTimer(function() 
            if (self.nps.inCombat) then
                return
            end

            for k, v in pairs(RoosterToolsDB.groups) do
                RoosterTools:GroupCleanup(k)
                RoosterTools:GroupReview(v)
            end
    
            RoosterTools:MainCeptionFix()
        end, 120)
    
        -- Every "interval" execute the update.
        local interval = 15 -- seconds
        self.npsTimer = self:ScheduleRepeatingTimer(function()
            if (self.nps.inCombat) then
                return
            end

            -- Update our current data with the latest from our current party.
            PartyWatch:UpdateDB()
        end, interval)
    
        self.syncTimer = self:ScheduleRepeatingTimer(function()
            if (self.nps.inCombat) then
                return
            end

            RoosterToolsComm:Send(RoosterToolsComm.TYPES.GUILD_SYNC_REQ, { 
                id = db.id, 
                refuseRankBelow = db.config.sync.refuseRankBelow 
            }, RoosterToolsComm.METHOD.GUILD)
        end, 200)

        self.minimapIcon:Show()
    end, 5)
end