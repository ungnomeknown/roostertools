RoosterToolsUtil = {}

function logDebug(msg)
    if (RoosterToolsDB.debug) then
        RoosterTools:Print(msg)
    end
end

function tablePrinter(t, depth)
    if (not t) then
        return
    end

    local depth = depth or 0
    local prepend = ""
    for i = 0, depth do
        prepend = prepend.."  "
    end

    for key, value in pairs(t) do
        if (type(value) == "table") then
            logDebug(prepend..key.." -> \n")
            tablePrinter(value, depth + 1)
        else
            logDebug(prepend..key.." -> "..tostring(value))
        end
    end
end

function string:split(sep)
    local sep, fields = sep or ":", {}
    local pattern = string.format("([^%s]+)", sep)
    self:gsub(pattern, function(c) fields[#fields+1] = c end)
    return fields
 end
 

RoosterToolsUtil.logDebug = logDebug
RoosterToolsUtil.tablePrinter = tablePrinter

RoosterToolsUtil.logInfo = function(msg) 
    RoosterTools:Print(msg)
end

function RoosterToolsUtil.count(t) 
    if (not t) then
        return 0
    end

    local i = 0
    for k, v in pairs(t) do
        i = i + 1
    end

    return i
end
