local rt = RoosterTools
local util = RoosterToolsUtil

RoosterToolsWaitList = {}
RoosterToolsWaitList.frame = CreateFrame("Frame", "RoosterToolsWaitListFrame", UIParent);

function RoosterToolsWaitList:Refresh()
    if (not rt.db.partyGuid or not rt.nps.guildRoster or RoosterToolsUtil.count(rt.nps.guildRoster) == 0) then
        self.table:SetData({}, true)
        self.frame:Hide()
        return
    end

    local currentGroup = rt.db.groups[rt.db.partyGuid]

    if (not currentGroup) then
        return
    end

    if (not currentGroup.waitlist) then
        currentGroup.waitlist = {}
    end

    local tableData = {}
    local removes = {}
    for k, v in pairs(currentGroup.waitlist) do
        local minutesWaited = (time() - v.time)/60

        -- Round the score to the nearest point per 15 minutes
        local mult = 10^0
        minutesWaited =  math.floor(minutesWaited * mult + 0.5) / mult

        -- Remove from waitlist if they are in the group.
        local nameOnly = v.name:gmatch("[^-]+")()
        for memberGuid, member in pairs(currentGroup.members) do
            if (member.unitName == v.name) then
                currentGroup.waitlist[k] = nil
                v = nil
            end
        end

        if (v) then
            local guildRecord = rt.nps.guildRoster[nameOnly]
            if (not guildRecord or not guildRecord.online) then
                util.logInfo("Offline "..v.name.." removing from wait list")
                currentGroup.waitlist[k] = nil
                v = nil
            end
        end

        if (v) then
            table.insert(tableData, {
                v.name,
                minutesWaited.."m"
            })
        end
    end

    if (RoosterToolsUtil.count(tableData) > 0) then
        self.frame:Show()
    else
        self.frame:Hide()
    end

    self.table:SetData(tableData, true)
end

function RoosterToolsWaitList:Add(playerName) 
    util.logInfo("Adding "..playerName.." to the wait list.")
    local currentGroup = rt.db.groups[rt.db.partyGuid]

    if (not currentGroup.waitlist) then
        currentGroup.waitlist = {}
    end

    for _, v in pairs(currentGroup.waitlist) do
        if (v.name == playerName) then
            return
        end
    end

    table.insert(currentGroup.waitlist, {
        ["name"] = playerName,
        ["time"] = time()
    })

    self:Refresh()
end

function RoosterToolsWaitList:Remove(playerName)

end

function RoosterToolsWaitList.CHAT_MSG_WHISPER(e, text, playerName)
    if (not rt.db.partyGuid) then
        util.logDebug("Player not in a party to waitlist to...")
        return
    end

    if (string.find(text, "waitlist") or string.find(text, "wait list")) then
        RoosterToolsWaitList:Add(playerName)
    end
end

function RoosterToolsWaitList:CreateFrame() 
    local frame = self.frame
    frame:SetFrameStrata("HIGH")

    -- Give the frame a background
    Mixin(frame, BackdropTemplateMixin)
    frame.backdropInfo = BACKDROP_TEXT_PANEL_0_16
    frame:OnBackdropLoaded()

    frame:SetMovable(true)
    frame:EnableMouse(true)
    frame:RegisterForDrag("LeftButton")
    frame:SetScript("OnDragStart", frame.StartMoving)
    -- frame:SetScript("OnDragStop", frame.StopMovingOrSizing)

    frame:SetWidth(200) -- Set these to whatever height/width is needed 
    frame:SetHeight(135) -- for your Texture

    if (rt.db.config.waitlist) then
        local x = rt.db.config.waitlist.x
        local y = rt.db.config.waitlist.y
        frame:SetPoint("TOPLEFT", x, y)
    else
        frame:SetPoint("TOPLEFT", 100, -100)
    end

    frame:SetScript("OnDragStop", function(...) 
        local _, _, _, x, y = frame:GetPoint("TOPLEFT")

        rt.db.config.waitlist.x = x
        rt.db.config.waitlist.y = y 

        frame.StopMovingOrSizing(...)
    end)

    local columns = {rt.gui.scrollingFrame.buildColumn({
        ["name"] = "Name",
        ["width"] = 110,
        ["align"] = "LEFT"
    }), rt.gui.scrollingFrame.buildColumn({
        ["name"] = "Wait",
        ["width"] = 40,
        ["align"] = "RIGHT"
    })}

    local st = LibStub("ScrollingTable"):CreateST(columns, 10, 10, rt.gui.COLORS.highlight, frame)
    RoosterToolsWaitList.table = st

    st:RegisterEvents({
        ["OnClick"] = function(rowFrame, cellFrame, data, cols, row, realrow, column, scrollingTable, ...)
            if (not realrow) then
                return false
            end

            local name = data[realrow][1]
            C_PartyInfo.InviteUnit(name)
        end
    })

end

function RoosterToolsWaitList:OnInitialize() 
    if (not rt.db.config.waitlist) then
        rt.db.config.waitlist = {
            ["enabled"] = true
        }
    end

    if (rt.db.config.waitlist.enabled) then
        rt:RegisterEvent("CHAT_MSG_WHISPER", RoosterToolsWaitList.CHAT_MSG_WHISPER)
        self:CreateFrame()

        self.waitTimer = rt:ScheduleRepeatingTimer(function()
            RoosterToolsWaitList:Refresh()    
        end, 2)

        self:Refresh()
    end
end