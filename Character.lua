RoosterToolsMember = {}

function RoosterToolsMember:New(unitId)
    local unitId = unitId

    local member = {}
    setmetatable(member, self)
    self.__index = self

    member.guid = UnitGUID(unitId)
    member.updated = time()
    member.unitName, member.unitRealm = UnitName(unitId)

    member.isAFK = UnitIsAFK(unitId)
    member.isConnected = UnitIsConnected(unitId)

    -- Distances
    member.withinInspectDistance = CheckInteractDistance(unitId, 1) -- 28 yards

    return member
end

function RoosterToolsMember:ToString() 
    local buff = ""
    for k, v in pairs(self) do
        buff = buff .. k .. " -> " .. tostring(v) .. "\n"
    end

    return buff
end

function RoosterToolsMember:Overlay(prev)
    local prev = prev
    local next = self

    if (not prev) then
        return next
    end

    -- Carry forward non-nil fields
    for k, v in pairs(prev) do
        if (not next[k] and type(next[k]) ~= "boolean") then
            next[k] = prev[k]
        end
    end

    return next
end