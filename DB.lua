local rt = RoosterTools
local db = {}

local defaultDb = {
    partyGuid = nil,
    config = {
        raid = {
            currentRaid = "Castle Nathria",
            minGuildMembers = 10
        },
        party = {
            minGuildMembers = 4
        },
        sync = {
            refuseRankBelow = 5 -- Below actually indicates greater given 0 is the highest guild rank. 
        }
    },
    -- Deprecated
    characters = {},

    -- Deprecated
    charactersIndex = {
        byName = {}
    },

    groups = {},
    mains = {},
    sync = {},
    ilvl = {},
    aliases = {},
    debug = false,
}

RoosterToolsDBConn = {}

function RoosterToolsDBConn:SelectGuildRaidGroups() 
    local t = {}
    
    for groupGuid, group in pairs(db.groups) do
        if (group.guildGroupTime) then
            for k, v in pairs(group.instances) do
                -- dump some of the weird raid events that have 'pvp' as the instance type. 
                if (v.type == "raid" and v.instanceName == db.config.raid.currentRaid) then
                    t[groupGuid] = v
                end
            end
        end
    end

    return t
end

function RoosterToolsDBConn:SelectGuildGroups() 
    local t = {}
    
    for k, v in pairs(db.groups) do
        if (v.guildGroupTime) then
            t[k] = v
        end
    end

    return t
end

function RoosterToolsDBConn:OnInitialize() 
    db = RoosterToolsDB

     -- Init the db if it hasn't been used.
     if (not db) then
        RoosterToolsDB = defaultDb
        db = RoosterToolsDB
    end

    if (not db.aliases) then
        -- DB Upgrade
        db.aliases = {}
    end

    if (not db.debug) then
        db.debug = false
    end

    if (not db.config.sync or not db.config.sync.refuseRankBelow) then
        db.config.sync = defaultDb.config.sync
    end

    -- Create a unique id for this addon. 
    if(not db.id) then
        db.id = time() .. rt.player.guid
    end

    if (not db.charactersIndex) then
        db.charactersIndex = {}
        db.charactersIndex.byName = {}

        -- Build initial index.
        for k, v in pairs(db.characters) do 
            if (v.unitName) then
                db.charactersIndex.byName[v.unitName] = k
            end
        end
    end

    if (not db.ilvl) then
        db.ilvl = {}
    end

    if (not db.config.raid.currentRaid) then
        db.config.raid.currentRaid = defaultDb.config.raid.currentRaid
    end

    db.info = nil
    db.groupsSerialized = nil
end