local rt = RoosterTools

-- Docs for lib-st 
-- https://www.wowace.com/projects/lib-st/pages/set-data/standard-dataset-format
-- Main RoosterTools Frame
local RoosterToolsFrame = CreateFrame("Frame", "RoosterToolsHolderFrame", UIParent);
RoosterToolsFrame:SetScale(1.15)

-- Attach the main frame to the global RoosterTools object
RoosterTools.rootFrame = RoosterToolsFrame

local function createScrollFrame()
    local frame = RoosterToolsFrame
    frame:SetFrameStrata("HIGH")
    frame.mainsOnly = true

    -- Give the frame a background
    Mixin(frame, BackdropTemplateMixin)
    frame.backdropInfo = BACKDROP_PARTY_32_32
    frame:OnBackdropLoaded()

    frame:SetMovable(true)
    frame:EnableMouse(true)
    frame:RegisterForDrag("LeftButton")
    frame:SetScript("OnDragStart", frame.StartMoving)
    frame:SetScript("OnDragStop", frame.StopMovingOrSizing)

    frame:SetWidth(700) -- Set these to whatever height/width is needed 
    frame:SetHeight(500) -- for your Texture
    frame:SetPoint("CENTER", 0, 0)
    frame:Hide()

    frame:SetScript("OnShow", function()
        C_GuildInfo.GuildRoster()
    end)

    local columns = {rt.gui.scrollingFrame.buildColumn({
        ["name"] = "#",
        ["width"] = 15
    }), rt.gui.scrollingFrame.buildColumn({
        ["name"] = "Rank",
        ["width"] = 100,
        ["align"] = "LEFT"
    }), rt.gui.scrollingFrame.buildColumn({
        ["name"] = "iLvl",
        ["width"] = 30,
        ["align"] = "LEFT"
    }), rt.gui.scrollingFrame.buildColumn({
        ["name"] = "Main",
        ["width"] = 100,
        ["align"] = "LEFT",
        ["color"] = function(data, cols, realrow, column, table)
            local main = data[realrow][4]

            for k, v in pairs(RoosterToolsDB.aliases) do
                if (v.alias == main) then
                    main = k
                    break
                end
            end

            local name = data[realrow][5]
            if (not name) then 
                RoosterToolsDB.mains[main] = nil
                return rt.gui.COLORS.red
            end
            local nameOnly = name:gmatch("[^-]+")()

            if (nameOnly == main) then
                return rt.gui.COLORS.green
            else
                return rt.gui.COLORS.red
            end
    
        end,
    }), rt.gui.scrollingFrame.buildColumn({
        ["name"] = "Name",
        ["width"] = 150,
        ["align"] = "LEFT",
    }), rt.gui.scrollingFrame.buildColumn({
        ["name"] = "Other",
        ["width"] = 50
    }), rt.gui.scrollingFrame.buildColumn({
        ["name"] = "Raids",
        ["width"] = 50
    }), rt.gui.scrollingFrame.buildColumn({
        ["name"] = "Absent",
        ["width"] = 50
    }), rt.gui.scrollingFrame.buildColumn({
        ["name"] = "Raid %",
        ["width"] = 50
    }), rt.gui.scrollingFrame.buildColumn({
        ["name"] = "Score",
        ["width"] = 50,
        ["color"] = function(data, cols, realrow, column, table)
            local score = data[realrow][10]

            if (score == 0) then
                return rt.gui.COLORS.red
            else
                return rt.gui.COLORS.green
            end
        end
    })}

    -- local tabardBackgroundUpper, tabardBackgroundLower, tabardEmblemUpper, tabardEmblemLower, tabardBorderUpper, tabardBorderLower = GetGuildTabardFileNames()

    local st = LibStub("ScrollingTable"):CreateST(columns, 25, 15, rt.gui.COLORS.highlight, frame)
    RoosterTools.guildRosterTable = st

    local characterFrame = CreateFrame("Frame", "RoosterToolsCharacterFrame", frame);
    -- Give the frame a background
    Mixin(characterFrame, BackdropTemplateMixin)
    characterFrame.backdropInfo = BACKDROP_DIALOG_32_32
    characterFrame:OnBackdropLoaded()

    characterFrame:SetWidth(300) -- Set these to whatever height/width is needed 
    characterFrame:SetHeight(500) -- for your Texture
    characterFrame:SetPoint("TOPLEFT", frame, "TOPRIGHT", 0, 0)
    characterFrame:Hide()

    local characterName = characterFrame:CreateFontString(characterFrame:GetName() .. "MainLabel", "OVERLAY",
                              "GameFontNormalLarge")
    characterName:SetText("")
    characterName:SetPoint("TOPLEFT", 20, -20)

    local aliasLabel = rt.gui.createLabel(characterFrame:GetName() .. "AliasLabel", characterFrame, "Alias", "TOPLEFT", characterName, "BOTTOMLEFT", 0, -10)
    local aliasText = rt.gui.createTextBox(characterFrame:GetName() .. "AliasText", characterFrame, "TOPLEFT", aliasLabel, "TOPRIGHT", 5, 0,
        function(parent, char) 
            local text = parent:GetText()
            if (text == "") then
                RoosterToolsDB.aliases[characterName:GetText()] = { alias = nil, updated = time() }
            else
                RoosterToolsDB.aliases[characterName:GetText()] = { alias = parent:GetText(), updated = time() }
            end

            RoosterTools:GuildRosterTableUpdate()
        end)

    local mainLabel = rt.gui.createLabel(characterFrame:GetName() .. "MainLabel", characterFrame, "Main", "TOPLEFT", aliasLabel, "BOTTOMLEFT", 20, -20)
    local mainText = rt.gui.createLabel(characterFrame:GetName() .. "MainLabel", characterFrame, "", "TOPLEFT", mainLabel, "TOPRIGHT", 5, 0)
    mainText:SetJustifyH("LEFT")

    st:RegisterEvents({
        ["OnClick"] = function(rowFrame, cellFrame, data, cols, row, realrow, column, scrollingTable, ...)
            if (not realrow) then
                return false
            end

            local name = data[realrow][5]
            name = name:gmatch("[^-]+")()

            characterFrame:Hide()

            local cName = name

            if (select(1, ...) == "RightButton") then
                cName = characterName:GetText()

                -- Add an alt
                if (cName == name) then
                    if (RoosterToolsDB.mains[cName]) then
                        RoosterToolsDB.mains[cName].name = nil
                        RoosterToolsDB.mains[cName].time = time()
                    end
                    mainLabel:SetText("")
                    mainText:SetText(name)
                else
                    RoosterToolsDB.mains[characterName:GetText()] = {name=name, time=time()}
                    mainLabel:SetText("Main")
                    mainText:SetText(name)
                end

                RoosterTools:GuildRosterTableUpdate()
            end

            characterName:SetText(cName)

            if (RoosterToolsDB.mains[cName] and RoosterToolsDB.mains[cName].name) then
                mainLabel:SetText("Main")
                mainText:SetText(RoosterToolsDB.mains[cName].name)
                aliasLabel:Hide()
                aliasText:Hide()
            else
                local alts = ""
                for k, v in pairs(RoosterToolsDB.mains) do
                    if (v.name == cName) then
                        alts = alts .. k .. "\n"
                    end
                end

                mainLabel:SetText("Alts")
                mainText:SetText(alts)
                aliasLabel:Show()

                local alias = RoosterToolsDB.aliases[characterName:GetText()]
                if (alias and alias.alias) then
                    aliasText:SetText(alias.alias)
                else
                    aliasText:SetText("")
                end
                aliasText:Show()
            end

            characterFrame:Show()
        end
    });

    local mainsOnlyCheckbox = rt.gui.createCheckbox("RoosterToolsHolderFrameMainsOnly", frame, "Mains", "Toggle mains only", "TOPRIGHT", -50, -15, function(frame, checkbox, state, a) 
        if (RoosterTools.rootFrame.mainsOnly) then
            RoosterTools.rootFrame.mainsOnly = false
        else
            RoosterTools.rootFrame.mainsOnly = true
        end

        RoosterTools:GuildRosterTableUpdate()
    end)

    mainsOnlyCheckbox:SetChecked(true)

    local guildMembersLabel = rt.gui.createLabel(characterFrame:GetName() .. "GuildMembers", frame, "Guild Mains", "TOPLEFT", frame, "BOTTOMLEFT", 15, 30)
    RoosterTools.rootFrame.guildMembers = rt.gui.createLabel(characterFrame:GetName() .. "GuildMembers2", frame, "-", "TOPLEFT", guildMembersLabel, "TOPRIGHT", 15, 0)

    rt.gui.createButton("RoosterToolsHolderFrameCloseButton", frame, 35, 175, "Close", "BOTTOMRIGHT", 0, 15,
        function(frame, button, down)
            RoosterToolsHolderFrame:Hide()
        end)
end

createScrollFrame()
