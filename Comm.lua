local rt = RoosterTools
local util = RoosterToolsUtil

-------------------------------------------------------------------------------
-- RoosterToolsComm 
--   All addon communications.
-------------------------------------------------------------------------------
RoosterToolsComm = {}

RoosterToolsComm.TYPES = {
    ACK = "ACK",

    -- Request from another guild user to receive data from this instance
    GUILD_SYNC_REQ = "RTSyncRequest",

    -- Request from another user for us to clear our sync data so they can receive again
    SYNC_CLEAR_REQ = "RTSyncClear",
    SYNC_CLEAR_RESP = "RTSyncClearResp",

    -- Version request
    VERSION_REQ = "RTVersion",
    VERSION_RESP = "RTVersionResp",

    -- Received sync data from another guild member. 
    SYNC_GROUP_RESP = "RTGroupResp",
    SYNC_MAIN_RESP = "RTMainResp",
    SYNC_ALIAS_RESP = "RTAliasResp",
    SYNC_ILVL_RESP = "RTIlvlResp",
}

RoosterToolsComm.METHOD = {
    WHISPER = "WHISPER",
    GUILD = "GUILD"
}

local function normalizeTarget(target) 
    -- Garbage in, garbage out.
    if (not target) then
        return nil
    end

    return target:gmatch("[^-]+")()
end

local function allowUpdate(prefix, target) 
    if (target) then
        if (target == rt.player.playerName) then
            return false
        end

        local guildMember = rt.nps.guildRoster[target]
        if (not guildMember) then
            util.logDebug("Not In Guild "..prefix.." from "..target)
            return false
        elseif (rt.db.config.sync.refuseRankBelow >= guildMember.rankIndex) then
            util.logDebug("Allowed "..prefix.." from "..target)
            return true
        else
            util.logDebug("No clue "..prefix.." from "..target)
            return false
        end
    else
        util.logDebug("Allowed "..prefix.." from unknown")
        return true
    end
end

local function mergeGroup(old, new) 
    old.created = math.min(old.created, new.created)

    old.updated = new.updated
    old.type = new.type

    if (old.guildGroupTime and new.guildGroupTime) then
        old.guildGroupTime = math.min(old.guildGroupTime, new.guildGroupTime)
    end

    if (new.instances) then
        for k, newInstance in pairs(new.instances) do
            -- There is a window where we could dupe an instance due to the inability to uniquely id it
            -- To avoid a collision, we'll just avoid taking extremely new data in the case of a sync.
            if (newInstance.created < (time() + 60)) then
                -- find the instance in old
                local foundInstance = nil
                for k, oldInstance in pairs(old.instances) do
                    if (oldInstance.instanceName == newInstance.instanceName 
                        and oldInstance.difficultyName == newInstance.difficultyName 
                        and oldInstance.updated and oldInstance.updated > newInstance.created) then
                        oldInstance.created = math.min(oldInstance.created, newInstance.created)
                        oldInstance.updated = newInstance.updated
                        foundInstance = oldInstance
                    end
                end

                if (not foundInstance) then
                    table.insert(old.instances, newInstance)
                end
            end
        end
    end

    for k, newEncounter in pairs(new.encounters) do
        -- There is a window where we could dupe an instance due to the inability to uniquely id it
        -- To avoid a collision, we'll just avoid taking extremely new data in the case of a sync.
        if (newEncounter.ended and newEncounter.started < (time() + 60)) then
            -- find the instance in old
            local foundEncounter = nil
            for k, oldEncounter in pairs(old.encounters) do
                if (oldEncounter.encounterName == newEncounter.encounterName 
                    and oldEncounter.difficultyID == newEncounter.difficultyID 
                    and oldEncounter.instance == newEncounter.instance 
                    and ((oldEncounter.started >= newEncounter.started and oldEncounter.started <= newEncounter.ended) or 
                         (newEncounter.started >= oldEncounter.started and newEncounter.started <= oldEncounter.ended))) then
                            oldEncounter.started = math.min(oldEncounter.started, newEncounter.started)
                            oldEncounter.ended = math.max(oldEncounter.ended, newEncounter.ended)
                    foundEncounter = oldEncounter
                end
            end

            if (not foundEncounter) then
                table.insert(old.encounters, newEncounter)
            end
        end
    end

    for k, newCharacter in pairs(new.members) do
        local oldCharacter = old.members[k]
        if (oldCharacter) then
            if (newCharacter.guildName) then
                oldCharacter.guildName = newCharacter.guildName
            end
            
            for k, newState in pairs(newCharacter.states) do
                local foundState = nil
                for k, oldState in pairs(oldCharacter.states) do
                    if (oldState.state == newState.state and oldState.time == newState.time) then
                        foundState = oldState.state
                        break
                    end
                end

                if (not foundState) then
                    table.insert(oldCharacter.states, newState)
                end
            end
        else
            old.members[k] = newCharacter
        end
    end
end

function RoosterToolsComm:Send(type, data, method, target, complete)
    -- skip sending messages to players that have went offline. 
    if (target) then
        local member = RoosterTools.nps.guildRoster[target]
        if (not member or not member.online) then
            return
        end
    end

    -- Never send empty data.
    if (not data) then
        data = {}
    end

    local serialized = rt:Serialize({
        ["version"] = 2,
        ["data"] = data
    })
    rt:SendCommMessage(type, serialized, method, target, "BULK", function(a, b, c)
        local to = target or method
        util.logDebug(to .. " " .. type .. " " .. b .. " " .. c)
        if (complete and b == c) then
            complete()
        end
    end)
end

function RoosterToolsComm:SendAck(k, prefix, updatedTime)
    -- Ack to the guild.
    local prefix = prefix or ""
    util.logDebug("Acking " .. prefix .. k )
    RoosterToolsComm:Send(RoosterToolsComm.TYPES.ACK, {
        id = rt.db.id,
        key = k, 
        prefix = prefix,
        time = updatedTime or time()
    }, self.METHOD.GUILD)
end

function RoosterToolsComm.GuildSyncReqHandler(prefix, data, dist, target, ...)
    if (rt.nps.inCombat) then
        return
    end

    local recv = time()

    local refuseRankBelow = data.refuseRankBelow
    if (refuseRankBelow and rt.player.guildRankIndex and refuseRankBelow < rt.player.guildRankIndex) then
        util.logDebug("Skipping sync request due to low guild rank from "..target .. " my rank " .. rt.player.guildRankIndex .. " expected " .. refuseRankBelow)
        return
    else
        local refuseRank = refuseRankBelow or "Unknown"
        util.logDebug("Syncing for " .. target .. " refused below rank " .. refuseRank)
    end

    local sync = rt.db.sync

    local syncIt = function(type, data, id, prefix)
        -- Use the id for sync data.
        if (not id) then
            util.logDebug("Didn't receive ID from ".. target)
            return
        end

        for k, v in pairs(data) do
            local key = id ..prefix..k
            local lastResponse = 0
            if (sync[key]) then
                lastResponse = sync[key].lastResponse
            end

            local completeFunc = function()
                -- Version 1 required a host side ack.
                if (data.version == 1) then
                    sync[key] = {
                        lastResponse = recv
                    }
                end
            end

            if (not rt.db.partyGuid or rt.db.partyGuid ~= k) then
                if ((v.updated and v.updated > lastResponse) or (v.time and v.time > lastResponse)) then
                    local t = {}
                    t[k] = v

                    -- RoosterToolsComm:Send(type, t, RoosterToolsComm.METHOD.WHISPER, target, completeFunc)
                    RoosterToolsComm:Send(type, t, RoosterToolsComm.METHOD.GUILD, nil, completeFunc)
                end
            end

            if (rt.nps.inCombat) then
                return
            end
        end
    end

    syncIt(RoosterToolsComm.TYPES.SYNC_GROUP_RESP, rt.db.groups, data.id, "")
    syncIt(RoosterToolsComm.TYPES.SYNC_MAIN_RESP, rt.db.mains, data.id, "m")
    syncIt(RoosterToolsComm.TYPES.SYNC_ALIAS_RESP, rt.db.aliases, data.id, "a")
    syncIt(RoosterToolsComm.TYPES.SYNC_ILVL_RESP, rt.db.ilvl, data.id, "ilvl")
end

function RoosterToolsComm.SyncGroupRespHandler(prefix, data, dist, target, ...)
    for k, v in pairs(data) do
        if (rt.nps.inCombat) then
            return
        end

        if (not rt.db.groups[k]) then
            util.logDebug("Add Group " .. k .. " from " .. target) 
            rt.db.groups[k] = v
        elseif (rt.db.groups[k].updated < v.updated and v.created < (time() + 60)) then
            util.logDebug("Update Group " .. k .. " from " .. target) 
            mergeGroup(rt.db.groups[k], v)
        end
        
        RoosterToolsComm:SendAck(k, nil, rt.db.groups[k].updated)
    end
end

function RoosterToolsComm.SyncMainRespHandler(prefix, data, dist, target, ...)
    for k, v in pairs(data) do
        if (rt.nps.inCombat) then
            return
        end

        if (not rt.db.mains[k]) then
            util.logDebug("Add Main " .. k .. " from " .. target) 
            rt.db.mains[k] = v
        elseif (rt.db.mains[k].time < v.time) then
            util.logDebug("Update Main " .. k .. " from " .. target) 
            rt.db.mains[k] = v
        end

        RoosterToolsComm:SendAck(k, "m", rt.db.mains[k].time)
    end
end

function RoosterToolsComm.SyncAliasRespHandler(prefix, data, dist, target, ...)
    for k, v in pairs(data) do
        if (rt.nps.inCombat) then
            return
        end

        if (not rt.db.aliases[k]) then
            util.logDebug("Add Alias " .. k .. " from " .. target) 
            rt.db.aliases[k] = v
        elseif (rt.db.aliases[k].updated < v.updated) then
            util.logDebug("Update Alias " .. k .. " from " .. target) 
            rt.db.aliases[k] = v
        end

        RoosterToolsComm:SendAck(k, "a", rt.db.aliases[k].updated)
    end
end

function RoosterToolsComm.SyncIlvlRespHandler(prefix, data, dist, target, ...)
    for k, v in pairs(data) do
        if (rt.nps.inCombat) then
            return
        end

        if (not rt.db.ilvl[k]) then
            util.logDebug("Add Ilvl " .. k .. " from " .. target) 
            rt.db.ilvl[k] = v
        elseif (rt.db.ilvl[k].updated < v.updated) then
            util.logDebug("Update Ilvl " .. k .. " from " .. target) 
            rt.db.ilvl[k] = v
        end

        RoosterToolsComm:SendAck(k, "ilvl", rt.db.ilvl[k].updated)
    end
end

function RoosterToolsComm.SyncClearReqHandler(prefix, data, dist, target, ...)
    -- Need the other party to post their id.
    if (not data.id) then
        return
    end

    local tbls = {
        { t = rt.db.ilvl, prefix = "ilvl" },
        { t = rt.db.groups, prefix = "" },
        { t = rt.db.mains, prefix = "m" },
        { t = rt.db.aliases, prefix = "a" }
    }

    for _, item in pairs(tbls) do
        for k, _ in pairs(item.t) do
            local key = data.id .. item.prefix .. k
            print("Clearing", key)
            rt.db.sync[key] = nil
        end
    end

    RoosterToolsComm:Send(RoosterToolsComm.TYPES.SYNC_CLEAR_RESP, {}, RoosterToolsComm.METHOD.WHISPER, target, function()
        util.logDebug("Sync cleared for "..target)
    end)
end

function RoosterToolsComm.SyncClearRespHandler(prefix, data, dist, target, ...)
    util.logInfo("Sync clear ack'd by "..target)
end

function RoosterToolsComm.VersionReqHandler(prefix, data, dist, target, ...)
    RoosterToolsComm:Send(RoosterToolsComm.TYPES.VERSION_RESP, {rtVersion = RoosterTools.version}, RoosterToolsComm.METHOD.WHISPER, target, function()
        util.logDebug("Responding to version req from "..target)
    end)
end

function RoosterToolsComm.VersionRespHandler(prefix, data, dist, target, ...)
    util.logInfo(target.." running "..data.rtVersion)
end

function RoosterToolsComm.Ack(prefix, data, dist, target, ...)
    -- Skip bad requests
    if (not data.key or not data.time) then
        util.logDebug("Bad Ack from "..target)
        return
    end

    local prefix = data.prefix or ""
    util.logDebug("Ack Received ".. prefix..data.key.. " from ".. target .. " "..data.id)
    rt.db.sync[data.id..prefix..data.key] = {
        lastResponse = data.time
    }
end

local eventHandlers = {
    [RoosterToolsComm.TYPES.ACK] = RoosterToolsComm.Ack,
    [RoosterToolsComm.TYPES.GUILD_SYNC_REQ] = RoosterToolsComm.GuildSyncReqHandler,
    [RoosterToolsComm.TYPES.SYNC_CLEAR_REQ] = RoosterToolsComm.SyncClearReqHandler,
    [RoosterToolsComm.TYPES.SYNC_CLEAR_RESP] = RoosterToolsComm.SyncClearRespHandler,
    [RoosterToolsComm.TYPES.SYNC_GROUP_RESP] = RoosterToolsComm.SyncGroupRespHandler,
    [RoosterToolsComm.TYPES.SYNC_MAIN_RESP] = RoosterToolsComm.SyncMainRespHandler,
    [RoosterToolsComm.TYPES.SYNC_ALIAS_RESP] =  RoosterToolsComm.SyncAliasRespHandler,
    [RoosterToolsComm.TYPES.SYNC_ILVL_RESP] =  RoosterToolsComm.SyncIlvlRespHandler,
    [RoosterToolsComm.TYPES.VERSION_REQ] =  RoosterToolsComm.VersionReqHandler,
    [RoosterToolsComm.TYPES.VERSION_RESP] =  RoosterToolsComm.VersionRespHandler,
}
function RoosterToolsComm.EventHandler(prefix, data, dist, target, ...)
    target = normalizeTarget(target)

    -- Skip handling request from the current player.
    if (target == rt.player.playerName) then
        return
    end
    
    local success, d = rt:Deserialize(data)
    if (not success) then
        return
    end

    if (type(d) ~= "table") then
        return
    end

    if (not allowUpdate(prefix, target)) then
        return
    end

    if (d.version and d.data and d.version >= 1 and d.data) then
        eventHandlers[prefix](prefix, d.data, dist, target, ...)
    else
        print(d.version, d.data)
    end
end

-- Register for comms
function RoosterToolsComm:OnInitialize()
    for k, v in pairs(self.TYPES) do
        rt:RegisterComm(v, RoosterToolsComm.EventHandler)    
    end
end
